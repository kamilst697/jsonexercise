package org.example;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws JSONException, IOException {
        List<Person> personList = generatePersonList();
        JSONArray jsonArray = convertArrayToJSON(personList);
        saveToFile("persons.json", jsonArray);
        List<Person> secondPersonList = convertJSONToList("persons.json");
        secondPersonList.forEach(System.out::println);
    }

    private static List<Person> generatePersonList() {
        return List.of(
                new Person("Jan", "Kowalski", Gender.MALE),
                new Person("Tomasz", "Nowacki", Gender.MALE),
                new Person("Anna", "Kowalska", Gender.FEMALE),
                new Person("Renata", "Malec", Gender.FEMALE)
        );
    }

    private static JSONArray convertArrayToJSON(List<Person> list) throws JSONException {
        JSONArray array = new JSONArray();
        for (Person person : list) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("firstName", person.getFirstName());
            jsonObject.put("lastName", person.getLastName());
            jsonObject.put("gender", person.getGender());
            array.put(jsonObject);
        }
        return array;
    }
    private static void saveToFile(String path, JSONArray jsonArray) throws IOException {
        BufferedWriter input = new BufferedWriter(new FileWriter(path));
        input.write(jsonArray.toString());
        input.close();
    }

    private static List<Person> convertJSONToList(String path) throws JSONException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(path));
        JSONArray jsonArray = new JSONArray(reader.readLine());
        List<Person> personList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String firstName = jsonObject.getString("firstName");
            String lastName = jsonObject.getString("lastName");
            Gender gender = Gender.valueOf(jsonObject.get("gender").toString());
            Person person = new Person(firstName, lastName, gender);
            personList.add(person);
        }
        return personList;
    }
}